const express = require('express')
const router = express.Router()
const auth = require('../middlewares/authToken')
const movie = require('../controllers/moviesController')
const maker = require('../controllers/makersController')
const review = require('../controllers/reviewsController')
const movieMaker = require('../controllers/juncMoviesMakersController')
const multer = require('../middlewares/multer')
const authAdmin = require('../middlewares/authAdmin')

router.post('/movies',multer.poster,authAdmin, movie.createMovie)
router.get('/movies/searchall',movie.getAllMovieAndSearch)
router.get('/movies/id',movie.getMovieByMovieId)
router.get('/movies/showfilter',movie.getMovieCategoriesAndTags)
router.put('/movies',authAdmin, multer.poster,movie.updateMovie)
router.delete('/movies',authAdmin, multer.poster,movie.deleteMovie)

router.post('/makers',authAdmin, multer.photo,maker.createMaker)
router.get('/makers',maker.getMakerByMakerId)
router.put('/makers',authAdmin, multer.photo,maker.updateMaker)
router.delete('/makers',authAdmin, multer.photo,maker.deleteMaker)

router.post('/reviews',auth,review.createReview)
router.get('/reviews/id',auth,review.getReviewByReviewId)
router.get('/reviews/beforelogin',review.getAllReviewBeforeLogin)
router.get('/reviews/afterlogin',auth,review.getAllReviewAfterLogin)
router.put('/reviews',auth,review.updateReview)
router.delete('/reviews',auth,review.deleteReview)

router.post('/moviesmakers',authAdmin, movieMaker.createMovieMaker)
router.put('/moviesmakers',authAdmin, movieMaker.updateMovieMaker)
router.delete('/moviesmakers',authAdmin, movieMaker.deleteMovieMaker)

module.exports = router;