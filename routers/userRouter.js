const express = require('express');
const router = express.Router();
const user = require('../controllers/userController.js');
const isAuthenticated = require('../middlewares/authToken.js')
const multer = require('../middlewares/multer')
const {check} = require('express-validator');
const Password = require('../controllers/password')

router.post('/register', user.create)
router.put('/uploadPhoto', multer.uploader, isAuthenticated, user.uploadPhoto)
router.post('/login', user.login)
router.put('/update', isAuthenticated, user.update)
router.get('/verify', user.verify)
router.get('/currentUser', isAuthenticated, user.currentUser)

//Password Reset
router.post('/recover', [
    check('email').isEmail().withMessage('Enter a valid email address'),
], Password.recover);

router.get('/reset/:token', Password.reset);

router.post('/reset/:token', [
    check('password').not().isEmpty().isLength({ min: 8 }).withMessage('Must be at least 8 chars long'),
    check('confirmPassword', 'Passwords do not match').custom((value, { req }) => (value === req.body.password)),
], Password.resetPassword);


module.exports = router;