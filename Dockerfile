FROM node:12.13-alpine

ARG env

WORKDIR /app
COPY . .

RUN NODE_ENV=$env npm install
EXPOSE 7777

CMD npm start