require('dotenv').config()
const express = require('express')
const app = express()
const morgan = require('morgan')
const mongoose = require('mongoose')
const swaggerUi = require('swagger-ui-express')
const swaggerDocument = require('./swagger.json')
const cors = require('cors')
const cons = require('consolidate')
var path = require("path")

const moviesRouter = require('./routers/moviesRouter')
const usersRouter = require('./routers/userRouter')

app.use(cors())
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument))

// view engine setup
app.engine('html', cons.swig)
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'html');

const env = process.env.NODE_ENV || 'development';
const dbConnection = {
  development: process.env.DB_CONNECTION,
  test: process.env.DB_CONNECTION_TEST,
  staging: process.env.DB_CONNECTION,
  production   : process.env.DB_CONNECTION,
}

// Connect to mongodb
mongoose.connect(dbConnection[env], {
    useNewUrlParser: true,
    useCreateIndex: true,
    useUnifiedTopology: true,
    useFindAndModify: false
  })
  .then(()=>{console.log(`Database connect to ${dbConnection[env]}`)})
  .catch(()=>{process.exit(1)})
  
if(env!=='test'){
    app.use(morgan('dev'))
}

// middleware
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use('/api/v1',moviesRouter);
app.use('/api/v1/users',usersRouter);
app.get('/', (req, res) => res.status(200)
    .send({
        status:true,
        data:'Congratulations your account has been verified'
    })
)

module.exports = app