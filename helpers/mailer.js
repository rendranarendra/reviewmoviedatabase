const nodemailer = require('nodemailer')
const sgTransport = require('nodemailer-sendgrid-transport');

const options = {
  auth: {
    api_key: process.env.SENDGRIDAPIKEY
  }
}
  
const client = nodemailer.createTransport(sgTransport(options));
module.exports={
  create:({to,subject,html})=>{
    return{
      from: 'no-reply@express.com',
      to,
      subject,
      html
    }
  },
  send:data=>{
    return client.sendMail(data);
  }
}