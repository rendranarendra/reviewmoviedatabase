const mailer = require('@sendgrid/mail')

mailer.setApiKey(process.env.SENDGRIDAPIKEY)

module.exports = mailer;