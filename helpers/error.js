exports.wrongUrl = (req, res) => {
    res.status(404).json({
        status: false,
        data: "Page not found"
    })
}

exports.internalServerError = (req, res) => {
    res.status(500).json({
        status: false,
        data: "Our server is on maintenance please try again later :)"
    })
}