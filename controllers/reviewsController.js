const Review = require('../models/reviewsModel')
const {success, error} = require('../helpers/response')
const Movie = require('../models/moviesModel')

async function createReview(req,res){
    let objReview = new Review()
    if(Object.prototype.hasOwnProperty.call(req.body,'review')){
        /* istanbul ignore next */
        objReview = {
            ...objReview,
            review:req.body.review
        }
    }
    try{
        const movie = await Movie.findById(req.body.movieId)
        if(!movie){
            return error(res,'Cannot find movie by this movie id',404)
        }
    }
    catch(ex){
        return error(res,ex,406)
    }
    const review = await Review.findOne({userId:req.user,movieId:req.body.movieId})
    if(review){
        return error(res,'User cannot review again',403)
    }
    objReview = {
        ...objReview,
        rating: parseInt(req.body.rating,10),
        userId: req.user,
        movieId: req.body.movieId
    }
    try{
        await Review.create(objReview)
        const sumReview = await Review.find({movieId:req.body.movieId})
        let sumRating = 0
        sumReview.forEach(i => {
            return sumRating+=i.rating
        })
        const avgRating = Math.round(sumRating/sumReview.length * 10) / 10
        await Movie.findByIdAndUpdate(req.body.movieId,{rating:avgRating},{ runValidators: true })
        return success(res,'Successfully create review and update rating movie',200)
    }
    catch(ex){
        return error(res,ex,422)
    }
}

async function getReviewByReviewId(req,res){
    try{
        const review = await Review.findById(req.query.id)
        .populate([
            {
                path:'userId',
                select:['name','image']
            }
        ])
        if(!review){
            return error(res,'Cannot find review by this review id',404)
        }
        return success(res,review,200)
    }
    catch(ex){
        return error(res,ex,406)
    }
}

async function getAllReviewBeforeLogin(req,res){
    try{
        const movie = await Movie.findById(req.query.movieId)
        if(!movie){
            return error(res,'Cannot find review by this movie id',404)
        }
    }
    catch(ex){
        return error(res,ex,406)
    }

    const review = await Review.find({movieId:req.query.movieId}).sort({updatedAt:'desc'})
    .populate([
        {
            path:'userId',
            select:['name','image']
        }
    ])
    return success(res,review,200)
}

async function getAllReviewAfterLogin(req,res){
    try{
        const movie = await Movie.findById(req.query.movieId).sort({updatedAt:'desc'})
        
        if(!movie){
            return error(res,'Cannot find movie by this movie id or ',404)
        }
    }
    catch(ex){
        return error(res,ex,406)
    }
    
    const checkReviewByUserId = await Review.findOne({userId:req.user,movieId:req.body.movieId})
    .populate([
        {
            path:'userId',
            select:['name','image']
        }
    ])
    if(!checkReviewByUserId){
        let review = await Review.find({movieId:req.query.movieId})
        .populate([
            {
                path:'userId',
                select:['name','image']
            }
        ])
        const infoReviewUser0 = [
            {buttonCreate: true},
            {editedMark: false}
        ]
        let allReview0 = {
            allDataReview:review,
            infoReviewUser:infoReviewUser0
        }
        return success(res,allReview0,200)
    }
    const allReviewExcludeUserId = await Review.find({movieId:req.query.movieId,userId:{$ne: req.user}})
    .populate([
        {
            path:'userId',
            select:['name','image']
        }
    ])
    let allDataReview = allReviewExcludeUserId
    allDataReview.unshift(checkReviewByUserId)
    const infoReviewUser = [
        {buttonCreate: false},
        {editedMark: checkReviewByUserId.createdAt.toString() === checkReviewByUserId.updatedAt.toString() ? true : false}
    ]
    const reviewUser = {
        allDataReview,
        infoReviewUser
    }
    return success(res,reviewUser,201)
}

async function updateReview(req,res){
    let objReview = {}
    if(Object.prototype.hasOwnProperty.call(req.body,'review')){
        objReview = {
            review:req.body.review
        }
    }
    if(Object.prototype.hasOwnProperty.call(req.body,'rating')){
        objReview = {
            ...objReview,
            rating:parseInt(req.body.rating,10)
        }
    }

    try{
        const review = await Review.findById(req.body.id)
        if(!review){
            return error(res,'Cannot find review by this review id',404)
        }
    }
    catch(ex){
        return error(res,ex,406)
    }

    try{
        const review = await Review.findByIdAndUpdate(req.body.id,objReview,{ runValidators: true })
        const sumReview = await Review.find({movieId:review.movieId})
        let sumRating = 0
        sumReview.forEach(i => {
            return sumRating+=i.rating
        })
        const avgRating = Math.round(sumRating/sumReview.length * 10) / 10
        await Movie.findByIdAndUpdate(review.movieId,{rating:avgRating},{ runValidators: true })
        return success(res,'Successfully update review and rating movie',200)
    }
    catch(ex){
        return error(res,ex,422)
    }
}

async function deleteReview(req,res){
    try{
        const review = await Review.findByIdAndDelete(req.body.id)
        const sumReview = await Review.find({movieId:review.movieId})
        let sumRating = 0
        sumReview.forEach(i => {
            return sumRating+=i.rating
        })
        let avgRating = Math.round(sumRating/sumReview.length * 10) / 10
        if(!avgRating){
            avgRating=0
        }
        await Movie.findByIdAndUpdate(review.movieId,{rating:avgRating},{ runValidators: true })
        return success(res,'Successfully delete review and update rating',200)
    }
    catch(ex){
        return error(res,ex,422)
    }
}

module.exports={
    createReview,
    getReviewByReviewId,
    getAllReviewBeforeLogin,
    getAllReviewAfterLogin,
    updateReview,
    deleteReview
}