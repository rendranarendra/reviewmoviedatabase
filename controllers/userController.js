require("dotenv").config();
const bcrypt = require("bcryptjs");
const User = require("../models/userModel.js");
const jwt = require("jsonwebtoken");
const { error, success } = require("../helpers/response.js");
const fs = require("fs");
const mailer = require("../helpers/mailer.js");
const Imagekit = require("imagekit");
const imagekit = new Imagekit({
  publicKey: process.env.PUBLIC_KEY,
  privateKey: process.env.PRIVATE_KEY,
  urlEndpoint: process.env.URL_ENDPOINT
});
const env = process.env.NODE_ENV 
// User register with token output
exports.create = async (req, res) => {
  try {
    const salt = await bcrypt.genSalt(10);
    const hash = await bcrypt.hash(req.body.password, salt);

    // update password to hash
    req.body.password = hash;

    const user = await User.create(req.body);
    let token = jwt.sign({ _id: user._id, role: user.role }, process.env.SECRET_KEY);

    if(env!=='development' & env!=='test'){
      // Email Confirmation
      let registerHTML = await fs.readFileSync(
        __dirname + "/../mailers/register.html",
        { encoding: "utf-8" }
      );
      registerHTML = registerHTML.replace("{User.Name}", user.name);
      registerHTML = registerHTML.replace(
        "{VerificationURL}",
        process.env.BASE_URL + "/api/v1/users/verify?token=" + token
      );

      let confirmation = await mailer.create({
        to: user.email,
        subject: "Email Verification",
        html: registerHTML
      });
      mailer.send(confirmation);
    }


    let data = {
      user,
      token
    };
    success(res, data, 200);
  } catch (err) {
    error(res, err, 400);
  }
};

// Verifying users by updating isConfirmed key to true
exports.verify = async (req, res) => {
  let token = req.query.token;

  try {
    let payload = await jwt.verify(token, process.env.SECRET_KEY);
    await User.findByIdAndUpdate(payload._id, { isConfirmed: true });
    success(res, payload, 200)
    res.redirect("/");
  } catch (err) {
    error(res, err, 401);
  }
};

exports.login = async (req, res) => {
  try {
    const token = await User.authenticate(req.body);
    success(res, token, 200);
  } catch (err) {
    error(res, err, 400);
  }
};

// Update user image profile using imagekit
exports.uploadPhoto = (req, res) => {
  imagekit
    .upload({
      file: req.file.buffer.toString("base64"),
      fileName: `IMG-${Date.now()}`
      
    })
    .then(data => {
      User.findOneAndUpdate(
        { _id: req.user },
        { image: data.url },
        { new: true }
      )
        .then(result => {
          success(res, result, 200);
        })
        .catch(err => {
          error(res, err, 422);
        });
    });
};

// Update username
exports.update = async (req, res) => {
  const { name } = req.body;
  try {
    const user = await User.findOneAndUpdate(
      { _id: req.user },
      { $set: { name } },
      { new: true }
    );

    success(res, user, 200);
  } catch (err) {
    error(res, err, 400);
  }
};

// Calling current user profile
exports.currentUser = async (req, res) => {
  try {
    let user = await User.findOne({ _id: req.user }).select("-password");
    success(res, user, 200);
  } catch (err) {
    error(res, err, 400);
  }
};

exports.forgotPassword = async (req, res) => {
  try {   
      let result = await User.forgotPassword(req.body)
      success(res, result, 201)
  }
  catch (err){
      error(res, err, 422)
  }
}