const ImageKit = require('imagekit')

const Movie = require('../models/moviesModel')
const MovieMaker = require('../models/juncMoviesMakersModel')
const {success, error} = require('../helpers/response');

const imageKit = new ImageKit({
    publicKey:process.env.PUBLIC_KEY_MOVIES,
    privateKey:process.env.PRIVATE_KEY_MOVIES,
    urlEndpoint:process.env.URL_ENDPOINT_MOVIES
})
const env = process.env.NODE_ENV 

async function createMovie(req,res){
    let movie = {}
    if(Object.prototype.hasOwnProperty.call(req.body,'trailer')){
        let trailerId = req.body.trailer.split('https://www.youtube.com/watch?v=')[1]
        movie = {
            trailer:'https://www.youtube.com/embed/'+trailerId
        }
    }
    if(Object.prototype.hasOwnProperty.call(req.body,'budget')){
        movie = {
            ...movie,
            budget:req.body.budget
        }
    }
    if(Object.prototype.hasOwnProperty.call(req.body,'tags')){
        movie = {
            ...movie,
            tags:req.body.tags
        }
    }
    if(Object.prototype.hasOwnProperty.call(req.body,'categories')){
        movie = {
            ...movie,
            categories:req.body.categories
        }
    }
    if(req.file.fieldname==='poster'&env!=='development'){
        let poster = await imageKit.upload({
            file:req.file.buffer.toString('base64'),
            fileName:`IMG-${Date.now()}`
        })
        if (poster){
            movie = {
                ...movie,
                poster:poster.url
            }
        }
    }
    movie = new Movie({
        ...movie,
        title:req.body.title,
        synopsis:req.body.synopsis,
        releaseDate:req.body.releaseDate
    })
    try{
        await Movie.create(movie)
        return success(res,'Successfully create movie',200)
    }
    catch(ex){
        return error(res,ex,422)
    }
}

async function getAllMovieAndSearch(req,res){
    let filter={}
    if(req.query.search){
        filter={
            title: { '$regex' : req.query.search, '$options' : 'i' } 
        }
    }
    if(req.query.tag){
        filter={
            ...filter,
            tags:'#'+req.query.tag
        }
    }
    if(req.query.category){
        filter={
            ...filter,
            categories:req.query.category
        }
    }
    const page =parseInt(req.query.page,10);
    const limit =parseInt(req.query.limit,10);
    const movie = await Movie.paginate(filter,{ page: page, limit: limit, sort:{ rating: 'desc', releaseDate:'desc' }})
    return success(res,movie,200)
}

function getMovieByMovieId(req,res){
    Movie.findById(req.query.id)
    .then(i=>{
        MovieMaker.find({movieId:i._id}).sort({role:'asc'})
        .select([
            'makerId',
            'role',
            'castName',
            'castRole'
        ])
        .populate([
            {
                path:'makerId',
                select:['name','description','photo','_id']
            }
        ])
        .then(maker=>{
            const cast = maker.map(data=>{
                if(data.role==='Cast'){
                    return {
                        castRole:data.castRole,
                        castName:data.castName,
                        makerId:data.makerId._id,
                        name:data.makerId.name,
                        photo:data.makerId.photo,
                        description:data.makerId.description
                    }
                }
            }).filter(x => !!x)
            const director = maker.map(data=>{
                if(data.role==='Director'){
                    return {
                        makerId:data.makerId._id,
                        name:data.makerId.name,
                        photo:data.makerId.photo,
                        description:data.makerId.description
                    }
                }
            }).filter(x => !!x)
            const producer = maker.map(data=>{
                if(data.role==='Producer'){
                    return {
                        makerId:data.makerId._id,
                        name:data.makerId.name,
                        photo:data.makerId.photo,
                        description:data.makerId.description
                    }
                }
            }).filter(x => !!x)
            const writer = maker.map(data=>{
                if(data.role==='Writer'){
                    return {
                        makerId:data.makerId._id,
                        name:data.makerId.name,
                        photo:data.makerId.photo,
                        description:data.makerId.description
                    }
                }
            }).filter(x => !!x)
            let movie ={
                ...i._doc,
                cast:cast,
                director:director,
                producer:producer,
                writer:writer
            }
            return success(res,movie,200)
        })
    })
    .catch(err=>{
        return error(res,err,422)
    })
}

function getMovieCategoriesAndTags(req,res){
    Movie.find()
    .select([
        'tags',
        'categories'
    ])
    .then(data=>{
        let output = {
            tags:[],
            categories:[]
        }
        data.forEach(i=>{
            if(i.tags.length>0){
                i.tags.forEach(j=>{
                    if(output.tags.includes(j)===false){
                        output.tags.push(j)
                    }     
                })
            }
            if(i.categories.length>0){
                i.categories.forEach(j=>{
                    if(output.categories.includes(j)===false){
                        output.categories.push(j)
                    }              
                })
            }  
        })
        return success(res,output,200)
    })
}

async function updateMovie(req,res){
    let movie = {}
    if(Object.prototype.hasOwnProperty.call(req.body,'title')){
        movie = {
            ...movie,
            title:req.body.title
        }
    }
    if(Object.prototype.hasOwnProperty.call(req.body,'synopsis')){
        movie = {
            ...movie,
            synopsis:req.body.synopsis
        }
    }
    if(Object.prototype.hasOwnProperty.call(req.body,'releaseDate')){
        movie = {
            ...movie,
            releaseDate:req.body.releaseDate
        }
    }   
    if(Object.prototype.hasOwnProperty.call(req.body,'trailer')){
        let trailerId = req.body.trailer.split('https://www.youtube.com/watch?v=')[1]
        movie = {
            ...movie,
            trailer:'https://www.youtube.com/embed/'+trailerId
        }
    }
    if(Object.prototype.hasOwnProperty.call(req.body,'budget')){
        movie = {
            ...movie,
            budget:req.body.budget
        }
    }
    if(Object.prototype.hasOwnProperty.call(req.body,'tags')){
        movie = {
            ...movie,
            tags:req.body.tags
        }
    }
    if(Object.prototype.hasOwnProperty.call(req.body,'categories')){
        movie = {
            ...movie,
            categories:req.body.categories
        }
    }
    if(req.file.fieldname==='poster'&env!=='development'){
        let poster = await imageKit.upload({
            file:req.file.buffer.toString('base64'),
            fileName:`IMG-${Date.now()}`
        })
        if (poster){
            movie = {
                ...movie,
                poster:poster.url
            }
        }
    }

    try{
        const checkMovieId = await Movie.findById(req.body.id)
        if(!checkMovieId){
            return error(res,'Cannot find movie by this movie id',404)
        }
    }
    catch(ex){
        return error(res,ex,406)
    }

    try{
        await Movie.findByIdAndUpdate(req.body.id,movie,{ runValidators: true })
        return success(res,'Successfully update movie',200)
    }
    catch(ex){
        return error(res,ex,422)
    }
}

async function deleteMovie(req,res){
    try{
        const checkMovieId = await Movie.findById(req.body.id)
        if(!checkMovieId){
            return error(res,'Cannot find movie by this movie id',404)
        }
    }
    catch(ex){
        return error(res,ex,406)
    }

    try{
        await Movie.findByIdAndDelete(req.body.id)
        return success(res,'Successfully delete movie',200)
    }    
    catch(ex){
        return error(res,ex,422)
    }
}

module.exports={
    createMovie,
    getAllMovieAndSearch,
    getMovieByMovieId,
    getMovieCategoriesAndTags,
    updateMovie,
    deleteMovie
}