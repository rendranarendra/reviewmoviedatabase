const MovieMaker = require('../models/juncMoviesMakersModel')
const {success, error} = require('../helpers/response')
const Maker = require('../models/makersModel')
const Movie = require('../models/moviesModel')

async function createMovieMaker(req,res){
    try{
        const checkMovieId = await Movie.findById(req.body.movieId)
        const checkMakerId = await Maker.findById(req.body.makerId)
        if((!checkMovieId)|(!checkMakerId)){
            return error(res,'Cannot find movie by this movie id or Cannot find maker by this maker id',404)
        }
    }
    catch(ex){
        return error(res,ex,406)
    }

    let movieMaker = {}
    if(Object.prototype.hasOwnProperty.call(req.body,'castRole')){
        movieMaker = {
            castRole:req.body.castRole
        }
    }
    if(Object.prototype.hasOwnProperty.call(req.body,'castName')){
        movieMaker = {
            ...movieMaker,
            castName:req.body.castName
        }
    }

    movieMaker= new MovieMaker({
        ...movieMaker,
        role:req.body.role,
        movieId:req.body.movieId,
        makerId:req.body.makerId
    })

    try{
        await MovieMaker.create(movieMaker)
        return success(res,'Successfully create conjunction movie and maker',200)
    }
    catch(ex){
        return error(res,ex,422)
    }  
}

async function updateMovieMaker(req,res){
    try{
        const checkMovieMakerId = await MovieMaker.findById(req.body.id)
        if((!checkMovieMakerId)){
            return error(res,'Cannot find movie maker by this id',404)
        }
    }
    catch(ex){
        return error(res,ex,406)
    }

    let movieMaker = {}
    if(Object.prototype.hasOwnProperty.call(req.body,'role')){
        movieMaker = {
            role:req.body.role
        }
    }
    if(Object.prototype.hasOwnProperty.call(req.body,'castRole')){
        movieMaker = {
            ...movieMaker,
            castRole:req.body.castRole
        }
    }
    if(Object.prototype.hasOwnProperty.call(req.body,'castName')){
        movieMaker = {
            ...movieMaker,
            castName:req.body.castName
        }
    }
    if(Object.prototype.hasOwnProperty.call(req.body,'movieId')){
        movieMaker = {
            ...movieMaker,
            movieId:req.body.movieId
        }
    }
    if(Object.prototype.hasOwnProperty.call(req.body,'makerId')){
        movieMaker = {
            ...movieMaker,
            makerId:req.body.makerId
        }
    }

    try{
        await MovieMaker.findByIdAndUpdate(req.body.id,movieMaker,{ runValidators: true })
        return success(res,'Successfully update movie maker',200)
    }
    catch(ex){
        return error(res,ex,422)
    }
}

async function deleteMovieMaker(req,res){
    try{
        const checkMovieMakerId = await MovieMaker.findById(req.body.id)
        if((!checkMovieMakerId)){
            return error(res,'Cannot find movie maker by this id',404)
        }
    }
    catch(ex){
        return error(res,ex,406)
    }

    try{
        await MovieMaker.findByIdAndDelete(req.body.id)
        return success(res,'Successfully delete movie maker',200)
    }
    catch(ex){
        return error(res,ex,422)
    }
}

module.exports={
    createMovieMaker,
    updateMovieMaker,
    deleteMovieMaker
}