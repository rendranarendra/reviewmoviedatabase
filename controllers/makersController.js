const ImageKit = require('imagekit')
const Maker = require('../models/makersModel')
const {success, error} = require('../helpers/response')
const MovieMaker = require('../models/juncMoviesMakersModel')

const imageKit = new ImageKit({
    publicKey:process.env.PUBLIC_KEY_MOVIES,
    privateKey:process.env.PRIVATE_KEY_MOVIES,
    urlEndpoint:process.env.URL_ENDPOINT_MOVIES
})
const env = process.env.NODE_ENV 

//controller admin
async function createMaker(req,res){
    let maker = {}
    if(Object.prototype.hasOwnProperty.call(req.body,'description')){
        maker = {
            description:req.body.description
        }
    }
    if(req.file.fieldname==='photo'&env!=='development'){
        let photo = await imageKit.upload({
            file:req.file.buffer.toString('base64'),
            fileName:`IMG-${Date.now()}`
        })
        if (photo){
            maker = {
                ...maker,
                photo:photo.url
            }
        }
    }
    maker = new Maker({
        ...maker,
        name:req.body.name
    })
    try{
        await Maker.create(maker)
        return success(res,'Successfully create maker',200)
    }
    catch(ex){
        return error(res,ex,422)
    }   
}

async function getMakerByMakerId(req,res){
    Maker.findById(req.query.id)
    .then(i=>{
        MovieMaker.find({makerId:i._id})
        .select([
            'movieId'
        ])
        .populate([
            {
                path:'movieId',
                select:['title','poster','releaseDate']
            }
        ])
        .then(movie=>{
            let maker ={
                ...i._doc,
                movie
            }
        return success(res,maker,200)
        })
    })
    .catch(err=>{
        return error(res,err,422)
    })
}

async function updateMaker(req,res){
    let maker = {}
    if(Object.prototype.hasOwnProperty.call(req.body,'name')){
        maker = {
            name:req.body.name
        }
    }
    if(Object.prototype.hasOwnProperty.call(req.body,'description')){
        maker = {
            ...maker,
            description:req.body.description
        }
    }
    if(req.file.fieldname==='photo'&env!=='development'){
        let photo = await imageKit.upload({
            file:req.file.buffer.toString('base64'),
            fileName:`IMG-${Date.now()}`
        })
        if (photo){
            maker = {
                ...maker,
                photo:photo.url
            }
        }
    }

    try{
        const checkMakerId = await Maker.findById(req.body.id)
        if(!checkMakerId){
            return error(res,'Cannot find maker by this maker id',404)
        }
    }
    catch(ex){
        return error(res,ex,406)
    }
    
    try{
        await Maker.findByIdAndUpdate(req.body.id,maker,{ runValidators: true })
        return success(res,'Successfully update maker',200)
    }
    catch(ex){
        return error(res,ex,422)
    }
}

async function deleteMaker(req,res){
    try{
        const checkMakerId = await Maker.findById(req.body.id)
        if(!checkMakerId){
            return error(res,'Cannot find maker by this maker id',404)
        }
    }
    catch(ex){
        return error(res,ex,406)
    }

    try{
        await Maker.findById(req.body.id)
        await Maker.findByIdAndDelete(req.body.id)
        return success(res,'Successfully delete maker',200)
    }    
    catch(ex){
        return error(res,ex,422)
    }
}

module.exports={
    createMaker,
    getMakerByMakerId,
    updateMaker,
    deleteMaker
}