const multer = require('multer')
const uploader = multer().single('image')
const photo = multer().single('photo')
const poster = multer().single('poster')
module.exports = {uploader,photo,poster}
