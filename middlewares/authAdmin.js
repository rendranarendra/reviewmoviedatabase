const jwt = require('jsonwebtoken');

async function authAdmin (req, res, next) {
  try{
    let str =req.headers.authorization;
    let token=str.split("Bearer ")[1];
    let payload = jwt.verify(token,process.env.SECRET_KEY);
    console.log("DEBUG", payload)
    if (payload.role !== "Admin")
        return res.status(401).json({
            status:false,
            errors:'You\'re not authorized to do this!'
        })
    next();
  }
  catch(err){
      return res.status(401).json({
          status:false,
          errors:'Invalid token'
      })
  }
}

module.exports = authAdmin;