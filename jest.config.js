const path = require('path')

module.exports = {
  collectCoverage: false,
  collectCoverageFrom: ['./controllers/**/*.js', './models/**/*.js', './routers/**/*.js', '!routes/index.js'],
  coverageDirectory: './public/coverage',
  testTimeout: 10000,
  testEnvironment: 'node',
  modulePathIgnorePatterns: [
    '__tests__/fixtures',
    '__tests__/CustomSequencer.js',
    'index.js'
  ],
  coveragePathIgnorePatterns: [
    '/node_modules/',
    'index.js'
  ],
  coverageReporters: ['lcov'],
  testSequencer: path.join(__dirname, '__tests__', 'CustomSequencer.js')
}