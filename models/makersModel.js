const mongoose = require('mongoose')

const Schema = mongoose.Schema

const makerSchema = new Schema({
    name:{
        type:'string',
        required:true,
        minlength:1,
        maxlength:20
    },
    description:{
        type:'string',
        minlength:1,
        maxlength:2000
    },
    photo:{
        type:'string',
        default: "https://i.stack.imgur.com/l60Hf.png"
    }
},
    {
        versionKey : false,
        timestamps : true
    }
)

const Maker = mongoose.model('Maker',makerSchema)
module.exports = Maker