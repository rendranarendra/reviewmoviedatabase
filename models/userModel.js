const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const crypto = require("crypto");

const validateEmail = function(email) {
  const re = /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(.\w{2,3})+$/;
  return re.test(email);
};

const userSchema = new Schema(
  {
    image: {
      type: String,
      optional: true,
      default: "https://i.stack.imgur.com/l60Hf.png"
    },
    name: {
      type: String,
      minlength: [3, "Username characters must be 3-32 characters"],
      maxlength: [32, "Username characters must be 3-32 characters"],
      required: true
    },
    role: {
      type: String,
      required: true,
      default: "User"
    },
    email: {
      type: String,
      unique: true,
      lowercase: true,
      trim: true,
      required: "Email field can't be empty",
      validate: [validateEmail, "Invalid email format!"]
    },
    isConfirmed: {
      type: Boolean,
      default: false
    },
    password: {
      type: String,
      minlength: 8,
      required: "Password can't be empty"
    },
    resetPasswordToken: String,
    resetPasswordExpires: String
  },
  {
    versionKey: false,
    timestamps: true
  }
);

// const User = mongoose.model("User", userSchema);
class User extends mongoose.model("User", userSchema) {
  static authenticate(data) {
    return new Promise((resolve, reject) => {
      User.findOne({ email: data.email }).then(user => {
        if (!user) return reject("User is incorrect");

        const validPassword = bcrypt.compareSync(data.password, user.password);
        if (!validPassword) return reject("User or password is incorrect");

        const token = jwt.sign(
          { _id: user._id, email: user.email, role:user.role },
          process.env.SECRET_KEY
        );
        resolve(token);
      });
    });
  }

  static generatePasswordReset(id) {
    let resetPasswordToken = crypto.randomBytes(20).toString("hex");
    let resetPasswordExpires = Date.now() + 3600000; // 60 minutes expired

    let properties = {
      resetPasswordToken: resetPasswordToken,
      resetPasswordExpires: resetPasswordExpires
    };

    return new Promise((resolve, reject) => {
      this.findByIdAndUpdate(id, properties, { new: true })
        .then(data => {
          resolve(data);
        })
        .catch(err => {
          reject(err);
        });
    });
  }
}

module.exports = User;
