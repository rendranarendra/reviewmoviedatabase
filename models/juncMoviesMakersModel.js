const mongoose = require('mongoose')

const Schema = mongoose.Schema

const juncMovieMakerSchema = new Schema({
    movieId:{
        type:Schema.Types.ObjectId,
        ref:'Movie'
    },
    makerId:{
        type:Schema.Types.ObjectId,
        ref:'Maker'
    },
    role:{
        type:Schema.Types.String,
        required:true,
        enum:['Cast','Producer','Writer','Director']
    },
    castName:{
        type:'string',
        minlength:1,
        maxlength:20
    },
    castRole:{
        type:'string',
        enum:['Main actor','Support actor','Voice actor']
    }
},
    {
        versionKey : false,
        timestamps : true
    }
)

const MovieMaker = mongoose.model('MovieMaker',juncMovieMakerSchema)
module.exports = MovieMaker