const mongoose = require('mongoose')

const Schema = mongoose.Schema

const reviewSchema = new Schema({
    review:{
        type:'string',
        minlength:0,
        maxlength:3000
    },
    rating:{
        type:'number',
        required:true,
        min:0,
        max:5
    },
    userId:{
        type:Schema.Types.ObjectId,
        ref:'User'
    },
    movieId:{
        type:Schema.Types.ObjectId,
        ref:'Movie'
    }
},
    {
        versionKey : false,
        timestamps : true
    }
)

const Review = mongoose.model('Review',reviewSchema)
module.exports = Review