const mongoose = require('mongoose')
require('mongoose-double')(mongoose)
const mongoosePaginate = require('mongoose-paginate-v2')

const Schema = mongoose.Schema
/* istanbul ignore next */
const validateReleaseDate = function(date) {
    if(!(date.length===10&&date.substr(2,1)==='-'&&date.substr(5,1)==='-')){
        return false
    }
    return true
  };

const movieSchema = new Schema({
    title:{
        type:'string',
        required:true,
        minlength:1,
        maxlength:60
    },
    synopsis:{
        type:'string',
        required:true,
        minlength:10,
        maxlength:2000
    },
    trailer:{
        type:'string',
        default:'https://www.youtube.com/embed/OCSnxXtjFL0'
    },
    poster:{
        type:'string',
        default:'https://i.stack.imgur.com/l60Hf.png'
    },
    rating:{
        type:Schema.Types.Double,
        min:0,
        max:5,
        default:0
    },
    releaseDate:{
        type:'string',
        required:true,
        validate: [validateReleaseDate, 'Invalid format release date, Format release date must be DD-MM-YYYY']
    },
    budget:{
        type:'string',
        minlength:1,
        maxlength:20,
        default:'0'
    },
    tags:[{
        type:Schema.Types.String
    }],
    categories:[{
        type:Schema.Types.String
    }]
},
    {
        versionKey : false,
        timestamps : true
    }
)
movieSchema.plugin(mongoosePaginate)

const Movie = mongoose.model('Movie',movieSchema)
module.exports = Movie