const supertest = require('supertest')
const app = require('../index')
const request = supertest(app)
const jwt = require('jsonwebtoken')
const Movie = require('../models/moviesModel')
const Review = require('../models/reviewsModel')
const User = require("../models/userModel.js")
const { generateUser } = require("./fixtures/userFixtures")
let user = generateUser()
let loginUser = {
  email:user.email,
  password:user.password
}
let user0 = generateUser()
let loginUser0 = {
  email:user0.email,
  password:user0.password
}
describe('Create user', () => {
  it('Should create a new user', async done => {
    request
        .post('/api/v1/users/register')
        .set('Content-Type', 'application/json')
        .send(JSON.stringify(user))
        .then(res => {
            let { status, data } = res.body
            expect(status).toBe(true)
            done()
        })
  })

  it('Should create a new user0', async done => {
    request
        .post('/api/v1/users/register')
        .set('Content-Type', 'application/json')
        .send(JSON.stringify(user0))
        .then(res => {
            let { status, data } = res.body
            expect(status).toBe(true)
            done()
        })
  })
})

describe('Feature Review',()=>{
  it('POST /api/v1/reviews success 200',async done=>{
    User.authenticate(loginUser)
      .then(res=>{
        let token = res
        let movie = require('./fixtures/movieFictures').create()
        let review = require('./fixtures/reviewFixtures').create()
        Movie.create(movie)
        .then(i=>{      
          let payload = jwt.verify(token,process.env.SECRET_KEY)
          review = {
            ...review,
            userId:payload._id,
            movieId:i._id
          }
          request
          .post('/api/v1/reviews')
          .set('Content-Type', 'application/json')
          .set('Authorization','Bearer '+token)
          .send(JSON.stringify(review))
          .then(res=>{
            const {status,data}=res.body
            expect(status).toBe(true)
            expect(res.statusCode).toEqual(200)
            expect(typeof data).toEqual('string')
            done()
          })
        })
      })
  })

  it('POST /api/v1/reviews error 406',async done=>{
    User.authenticate(loginUser)
      .then(res=>{
        let token = res
        let movie = require('./fixtures/movieFictures').create()
        let review = require('./fixtures/reviewFixtures').create()
        Movie.create(movie)
        .then(i=>{      
          let payload = jwt.verify(token,process.env.SECRET_KEY)
          review = {
            ...review,
            userId:payload._id,
            movieId:'404'
          }
          request
          .post('/api/v1/reviews')
          .set('Content-Type', 'application/json')
          .set('Authorization','Bearer '+token)
          .send(JSON.stringify(review))
          .then(res=>{
            const {status,errors}=res.body
            expect(status).toBe(false)
            expect(res.statusCode).toEqual(406)
            expect(typeof errors).toEqual('object')
            done()
          })
        })
      })
  })

  it('POST /api/v1/reviews error 404',async done=>{
    User.authenticate(loginUser)
      .then(res=>{
        let token = res
        let movie = require('./fixtures/movieFictures').create()
        let review = require('./fixtures/reviewFixtures').create()
        Movie.create(movie)
        .then(i=>{      
          let payload = jwt.verify(token,process.env.SECRET_KEY)
          let falseMovie = new Movie(movie)
          review = {
            ...review,
            userId:payload._id,
            movieId:falseMovie._id
          }
          request
          .post('/api/v1/reviews')
          .set('Content-Type', 'application/json')
          .set('Authorization','Bearer '+token)
          .send(JSON.stringify(review))
          .then(res=>{
            const {status,errors}=res.body
            expect(status).toBe(false)
            expect(res.statusCode).toEqual(404)
            expect(typeof errors).toEqual('string')
            done()
          })
        })
      })
  })

  it('POST /api/v1/reviews error 422',async done=>{
    User.authenticate(loginUser0)
      .then(res=>{
        let token = res
        let movie = require('./fixtures/movieFictures').create()
        Movie.create(movie)
        .then(i=>{      
          let payload = jwt.verify(token,process.env.SECRET_KEY)
          review = {
            rating:10,
            userId:payload._id,
            movieId:i._id
          }
          request
          .post('/api/v1/reviews')
          .set('Content-Type', 'application/json')
          .set('Authorization','Bearer '+token)
          .send(JSON.stringify(review))
          .then(res=>{
            const {status,errors}=res.body
            expect(status).toBe(false)
            expect(res.statusCode).toEqual(422)
            expect(typeof errors).toEqual('object')
            done()
          })
        })
      })
  })



})