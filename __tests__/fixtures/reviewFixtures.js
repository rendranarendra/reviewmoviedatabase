const faker = require('faker')

function create(){ 
    let num = (Math.floor(Math.random() * Math.floor(5)).toString())
    return {
        review: faker.random.word,
        rating: num,
    }
}

module.exports = {create}