const faker = require('faker')

exports.generateUser = function () {
  return {
    name: faker.name.findName(),
    email: faker.internet.email(),
    password: faker.internet.password(8)
  }
}
