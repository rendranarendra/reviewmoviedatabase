const faker = require('faker')

function create(){ 
    let num = (Math.floor(Math.random() * Math.floor(5)).toString())
    return {
        name:faker.name.title,
        description:faker.random.word,
        photo:faker.image.imageUrl
    }
}

module.exports = {create}