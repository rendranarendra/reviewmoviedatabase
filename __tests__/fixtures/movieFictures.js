const faker = require('faker')
const moment = require('moment')

function create(){ 
    let num = (Math.floor(Math.random() * Math.floor(5)).toString())
    return {
        title:faker.name.title,
        synopsis:faker.random.words,
        trailer:'https://www.youtube.com/watch?v=AmRua5k2UF0',
        rating: num,
        releaseDate: moment().add(7,'days').format('DD-MM-YYYY'),
        budget: '1000',
        tags:[
            '#'+faker.random.word,
            '#'+faker.random.word,
            '#'+faker.random.word,
            '#'+faker.random.word
        ],
        categories:[
            faker.random.word,
            faker.random.word,
            faker.random.word,
            faker.random.word
        ]
    }
}

module.exports = {create}