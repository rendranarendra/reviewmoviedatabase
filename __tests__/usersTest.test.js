const fs = require('fs')
const supertest = require("supertest");
const mongoose = require("mongoose");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");
require("dotenv").config();

const User = require("../models/userModel");

const { generateUser } = require("./fixtures/userFixtures");
const app = require("../index");
const request = supertest(app);

let user = generateUser();

async function removeAllCollections() {
  const collections = Object.keys(mongoose.connection.collections);
  for (const collectionName of collections) {
    const collection = mongoose.connection.collections[collectionName];
    await collection.deleteMany();
  }
}

async function dropAllCollections() {
  const collections = Object.keys(mongoose.connection.collections);
  for (const collectionName of collections) {
    const collection = mongoose.connection.collections[collectionName];
    try {
      await collection.drop();
    } catch (error) {
      // Sometimes this error happens, but you can safely ignore it
      if (error.message === "ns not found") return;
      // This error occurs when you use it.todo. You can
      // safely ignore this error too
      if (error.message.includes("a background operation is currently running"))
        return;
      console.log(error.message);
    }
  }
}

beforeAll(async done => {
  mongoose
    .connect(process.env.DB_CONNECTION_TEST, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useCreateIndex: true,
      useFindAndModify: false
    })
    .then(() => {
    //   console.log("connected");
    })
    .catch(err => console.error(err));
  await removeAllCollections();
  done();
});

// Connection should be close, otherwise jest will show a warning
// see https://github.com/facebook/jest/issues/7092#issuecomment-429049494

// Disconnect Mongoose
afterAll(async done => {
  await dropAllCollections();
  await mongoose.connection.close();
  done();
});

describe("User endpoint", () => {
  it("Create a new user", async done => {
    const res = await request
      .post("/api/v1/users/register")
      .send(Object.assign(user, { role: "Admin" }));
    const { status, data } = res.body;

    expect(status).toBe(true);
    expect(res.statusCode).toEqual(200);
    expect(typeof data).toEqual("object");
    expect(data).toHaveProperty("token");
    expect(true).toEqual(true);
    done();
  });

  // it("Should activate the corresponding user", done => {
  //   User.authenticate({ email: user.email, password: user.password }).then(
  //     user => {
        
  //       request.get(`/api/v1/users/verify?token=${user}`).then(res => {
  //         const { status, data } = res.body;
  //         console.log(res.body)
  //         expect(res.body.status).toBe(true);
  //         expect(typeof data).toEqual("object");
  //         done();
  //       });
  //     }
  //   );
  // });

  // it("Should not activate due to invalid token", done => {
  //   const token = jwt.sign({ noid: "error test", }, process.env.SECRET_KEY);
  //   request.get(`/api/v1/users/verify?token=${token}`).then(res => {
  //     const { status, message } = res.body;
  //     expect(status).toBe(undefined);
  //     expect(typeof message).toEqual("undefined");
  //     done();
  //   });
  // });

  // it("Should not activate: user not found", done => {
  //   const mail = generateUser().email;
  //   const pass = generateUser().password;
  //   const salt = bcrypt.genSaltSync(10);
  //   const encrypted_password = bcrypt.hashSync(pass, salt);
  //   let token = "";
  //   User.create({ name: user.name, email: mail, encrypted_password })
  //     .then(user => {
  //       return User.authenticate({ email: mail, password: pass });
  //     })
  //     .then(login => {
  //       token = login.token;
  //       return User.findByIdAndDelete(login._id);
  //     })
  //     .then(deleted => {
  //       request.get(`/api/v1/users/activate?token=${token}`).then(res => {
  //         const { status, message } = res.body;
  //         expect(status).toBe(false);
  //         expect(typeof message).toEqual("string");
  //         done();
  //       });
  //     });
  // });

  // it("Login the created user", done => {
  //   request
  //     .post("/api/v1/users/login")
  //     .set("Content-Type", "application/json")
  //     .send({ email: user.email, password: user.password })
  //     .then(res => {
  //       const { status, data } = res.body;

  //       expect(status).toBe(true);
  //       expect(res.statusCode).toEqual(200);
  //       expect(typeof data).toEqual("string");
  //       expect(true).toEqual(true);
  //       done();
  //     });
  // });

  // it.only("Should get the current user", done => {
  //   request
  //     .post("/api/v1/users/login")
  //     .set("Content-Type", "application/json")
  //     .send({ email: user.email, password: user.password })
  //     .then(temp => {
  //       const token = temp.body.data;
  //       request
  //         .get("/api/v1/users/currentUser")
  //         .set("Authorization", "Bearer " + token)
  //         .then(res => {
  //           const { status, data } = res.body;
  //           expect(status).toBe(true);
  //           expect(res.statusCode).toEqual(200);
  //           expect(typeof data).toEqual("object");
  //           expect(true).toEqual(true);
  //           done();
  //         });
  //     });
  // });

  // it("Should update user's image", done => {
  //   User.authenticate({ email: user.email, password: user.password }).then(
  //     data => {
  //       request
  //         .put("/api/v1/users/uploadPhoto")
  //         .set("Authorization", "Bearer " + data.token)
  //         .set("Content-Type", "multipart/form-data")
  //         .attach("image", ('../images/TestImage.jpg'))
  //         .then(res => {
  //           expect(res.status).toBe(200);
  //           done();
  //         });
  //     }
  //   );
  // });

  it("Should update username only", done => {
    User.authenticate({ email: user.email, password: user.password }).then(
      data => {
        request
          .put("/api/v1/users/update")
          .set("Authorization", "Bearer " + data)
          .send({ name: generateUser().name })
          .then(res => {
            expect(res.status).toBe(200);
            done();
          })
          .catch(err => {
            console.log(err);
          });
      }
    );
  });

  describe("Negative test: User", () => {
    it("Should not create user: empty password", done => {
      request
        .post("/api/v1/users/register")
        .send({ name: user.name })
        .then(res => {
          expect(res.status).toBe(400);
          expect(typeof res.body).toEqual("object");
          done();
        });
    });
  });
    it("Should not create user: empty email", done => {
      request
        .post("/api/v1/users/register")
        .send({ name: user.name, password: user.password })
        .then(res => {
          expect(res.status).toBe(400);
          expect(typeof res.body).toEqual("object");
          done();
        });
    });

    it("Should not login user: empty email", done => {
      request
        .post("/api/v1/users/login")
        .send({ password: user.password })
        .then(res => {
          expect(res.status).toBe(400);
          expect(typeof res.body).toEqual("object");
          done();
        });
    });

    it("Should not login user: wrong email or password", done => {
      request
        .post("/api/v1/users/login")
        .send({ email: user.email, password: "testfailedlogin" })
        .then(res => {
          expect(res.status).toBe(400);
          expect(typeof res.body).toEqual("object");
          done();
        });
    });
  });